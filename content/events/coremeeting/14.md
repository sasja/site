---
startdate:  2019-02-09
starttime: "13:00"
linktitle: "Coremeeting 14"
title: "Coremeeting 14"
location: "HSBXL"
eventtype: "Meeting"
price: ""
series: "coremeeting"
---

Our monthly CoreMeeting. Discussing main issues like the bookkeeping.  
Exceptionally not on the first Saturday of the month because Bytenight.
