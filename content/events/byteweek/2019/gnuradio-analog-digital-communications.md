---
startdate:  2019-01-31
starttime: "20:00"
enddate:  2019-01-31
allday: false
linktitle: "Using GNU Radio for Analog and Digital Communications"
title: "Using GNU Radio for Analog and Digital Communications"
location: "HSBXL"
eventtype: "Workshop"
price: "Free"
series: "byteweek2019"
image: "gnuradio.png"
alias: [events/byteweek/gnuradio-amateur-radio]
--- 

Using GNU Radio for Analog and Digital Communications  
https://www.meetup.com/hackerspace-Brussels-hsbxl/events/258061306/