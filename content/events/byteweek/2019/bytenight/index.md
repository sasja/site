---
startdate:  2019-02-02
starttime: "18:00"
linktitle: "Bytenight"
title: "Bytenight 2019"
location: "HSBXL"
eventtype: "Thé dansant"
price: ""
image: "bytenight.png"
series: "byteweek2019"
aliases: [/bytenight/]
---


As an after-party to the big FOSDEM Free Software conference in Brussels,  
the oldest Hackerspace in town organises its annual free party.

# 10^3

- 10 years HSBXL
- 10 years .be hackerspaces
- 10 years Bytenight
- (5th location)
- (500th Techtuesday)

![Party!](party.jpg "Party!")


# Music
Weird music for weird people.

* CIRC8
* [Jonny and the Bomb](https://soundcloud.com/4a-6f-6e-6e-79-20-2b-20-5)
* [3D63](https://soundcloud.com/3d63)
* [planète concrète](http://planeteconcrete.tumblr.com/) pesence/interface

# Unsupported, deprecated versions
- [Bytenight v2018](https://wiki.hsbxl.be/Bytenight_2018)
- [Bytenight v2017](https://wiki.hsbxl.be/Bytenight_2017)
- [Bytenight v2016](https://wiki.hsbxl.be/Bytenight_(2016))
- [Bytenight v2015](https://wiki.hsbxl.be/Bytenight_(2015))
- [Bytenight v2014](https://wiki.hsbxl.be/Bytenight_(2014))
- [Bytenight v2013](https://wiki.hsbxl.be/Bytenight_2013)
- [Bytenight v2012](https://wiki.hsbxl.be/ByteNight_(2012))
- [Bytenight v2011](https://wiki.hsbxl.be/ByteNight_(2011))
- [Bytenight v2010](https://wiki.hsbxl.be/ByteNight_(2010))


# Organizing
Notes can be found on https://etherpad.openstack.org/p/bytenight2019

# Supporting
Please support this event through promotion via our official social media pages
- [Meetup](https://www.meetup.com/hackerspace-Brussels-hsbxl/events/258017972/)


# NEW: Fri 1st February, Bitnight
In addition to Saturday's Bytenight, there will be a smaller social event on Friday, [Bitnight](https://hsbxl.be/events/byteweek/2019/bitnight/). 

This will mix workshop and hackathon participants from that day with early FOSDEM visitors coming to Brussels.

All FOSS friends welcome

See Byteweek for details on HSBXL's pre-FOSDEM events:
[https://hsbxl.be/events/byteweek/2019/](https://hsbxl.be/events/byteweek/2019/)
