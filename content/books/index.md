---
title: "HSBXL Public Books"
linktitle: "HSBXL Books"
---

    This is a demo of what public bookkeeping could be.  
    This is fictive data only for demo purposes!  


<div class="accordion">

<h1 class="header">˅ 2018 </h2>
<div>
<h2>Incoming</h2>
{{< books_incoming year="2018">}}
<h2>Outgoing</h2>
{{< books_outgoing year="2018" >}}
</div>
<h1 class="header">˅ 2017 </h2>
<div>
<h2>Incoming</h2>
{{< books_incoming year="2017">}}
<h2>Outgoing</h2>
{{< books_outgoing year="2017" >}}
</div>
<h1 class="header">˅ 2016</h2>
<div>
<h2>Incoming</h2>
{{< books_incoming year="2016">}}
<h2>Outgoing</h2>
{{< books_outgoing year="2016" >}}

</div>


</div>

# Bank account
The amount on the HSBXL bank account at **{{< warchest date >}}** was **€{{< warchest amount >}}**.
